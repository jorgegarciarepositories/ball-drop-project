﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoalScript : MonoBehaviour {

    public ParticleSystem psGoal;
    public GameObject goal;
    private float min;
    private float max;
    public bool winner = false;

    public static bool w1 = false;
    public static bool w2 = false;
    public static bool w3 = false;

    public static bool unlock1 = false;
    public static bool unlock2 = false;
    public static bool unlock3 = false;

    private Scene scene;

    private void Start()
    {
        min = transform.position.z;
        max = transform.position.z + 8;
    }
    private void Update()
    {
        // Check if 'R' has been clicked
        scene = SceneManager.GetActiveScene();

        if (scene.name == "World 2")
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.PingPong(Time.time * 2, max - min) + min);
        }

        if (Input.GetKey(KeyCode.R))
        {
            winner = false;
        }
    }
    private void OnTriggerEnter()
    {
        scene = SceneManager.GetActiveScene();

        
        // Goal has been triggered

        if (goal.CompareTag("Goal"))
        {
        
            winner = true;
            psGoal.Play();
        }
        if (scene.name == "World 1")
        {
            w1 = true;
            unlock1 = increasePoints(unlock1);
        }
        else if (scene.name == "World 2")
        {
            w2 = true;
            unlock2 = increasePoints(unlock2);
        }
        else if (scene.name == "World 3")
        {
            w3 = true;
            unlock3 = increasePoints(unlock3);
        }
    }
    private bool increasePoints(bool world)
    {
        if (!world)
        {
            GameObject.Find("GameController").GetComponent<GameControllerScript>().Score();
            world = true;
        }
        return world;
    }
}
